import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'TestTime.dart';
class BookingList extends StatefulWidget
{
  @override
  State<StatefulWidget> createState() {
    return BookingView();
  }
}

class BookingView extends State<BookingList>
{
  @override
  Widget build(BuildContext context)
  {
   return new Scaffold(
    backgroundColor: Colors.grey,
     body:  new ListView(

    padding: EdgeInsets.all(8.0),
    shrinkWrap: true,
    children:
      [
        new Container(
          child: new Card(elevation: 4.0,
            child: new Container(
              //padding: EdgeInsets.all(20.0),
              child:  new Row(
                children: [
                  Expanded(flex: 3,
                    child: new Container(
                      child: new Container(
                          height: 80.0,
                          decoration: new BoxDecoration(
                              image: new DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage('drawable/a4.jfif'),
                              )
                          )
                      ),
                    ),),
                  SizedBox(width: 20,),
                  Expanded(flex: 7,child: new Container(
                    padding: EdgeInsets.all(20.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Welcome",style: TextStyle(fontSize: 20,color: Colors.red,fontWeight: FontWeight.bold),),
                        SizedBox(height: 5,),
                        Text("Book last minute appointment ,pay your doctor's prescription on tha all new app",style: TextStyle(fontSize: 12,color: Colors.grey),),
                        SizedBox(height: 5,),
                      ],
                    ),
                  ),),
                ],
              ),
            ),),
        ),

        SizedBox(height: 20,),
        new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.all_inclusive,color: Colors.red[900],),

            SizedBox(width: 10,),
            Text("Doctors In Pune",style: TextStyle(fontSize: 20),),
          ],
        ),

        SizedBox(height: 20,),
        getListing()
      ]
    ),
   );
  }

  @override
  void initState() {
    RefreshList_items.add(new DataModel("Dr. Nema","Gynalogist","kothrud","a1.jpeg"));
    RefreshList_items.add(new DataModel("Cabil Children Clinic","Pediarition","Baner","a3.jfif"));
    RefreshList_items.add(new DataModel("Dr. Rashmi GapChup","Gynalogist","kothrud","a2.jpeg"));
  }

  List<DataModel> RefreshList_items=new List();
  getListing()
  {
    return new ListView.builder(
        itemCount: RefreshList_items.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (BuildContext ctxt, int index) {
          return new InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => TestTime()));
            },
            child: new Container(
              child: new Card(elevation: 4.0,
              child: new Container(
                padding: EdgeInsets.all(20.0),
                child:  new Row(
                  children: [
                    Expanded(flex: 3,
                      child: new Container(
                        child: new Container(
                            height: 80.0,
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: AssetImage('drawable/'+RefreshList_items[index].image),
                                )
                            )
                        ),
                      ),),
                    SizedBox(width: 20,),
                    Expanded(flex: 7,child: new Container(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(RefreshList_items[index].name,style: TextStyle(fontSize: 18,color: Colors.black),),
                          SizedBox(height: 5,),
                          Text(RefreshList_items[index].profession,style: TextStyle(fontSize: 12,color: Colors.grey),),
                          SizedBox(height: 5,),
                          new Row(
                            children: [
                              Expanded(
                                child:  Text(RefreshList_items[index].area,style: TextStyle(fontSize: 14,color: Colors.grey),),),

                              Expanded(child: new Row(
                                children: [Icon(Icons.calendar_today,size: 12,),Text("  Go Cashless",style: TextStyle(fontSize: 10),)],
                              ),),
                            ],
                          ),
                        ],
                      ),
                    ),),
                  ],
                ),
              ),),
            ),
          );
        });
  }
}

class DataModel
{
  String name,profession,area,image;
  DataModel(this.name, this.profession, this.area,this.image);
}