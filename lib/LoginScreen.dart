import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/DashBoardActivity.dart';

//import 'LoginPackage/Global.dart';
import 'dart:convert';

import 'GlobalFile.dart';


class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeApp();
  }
}

class HomeApp extends State<LoginScreen> {
  TextEditingController _email = new TextEditingController();
  TextEditingController _pass = new TextEditingController();
  GlobalKey<FormState> _formkey = new GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  final focus = FocusNode();
  String _message = 'Log in/out by pressing the buttons below.';

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return
      MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
          top: false,
          bottom: false,
          child: new Scaffold(
              key: _globalKey,
              body: new Form(
                key: _formkey,
                child: Stack(
                  children: <Widget>[
                    Center(
                      child:Container(

                        child:  new Image.asset(
                          'drawable/login_bgs.jpg',
                          width: size.width,
                          height: size.height,
                          fit: BoxFit.fill,
                        ),
                      )
                    ),
                    Center(
                        child: new Container(

                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * .05,
                              right: MediaQuery.of(context).size.width * .05,
                            ),
                            child: new Center(
                              child: new ListView(
                                  shrinkWrap: true,
                                  children: <Widget>[
                                    Center(
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            SizedBox(
                                              height: MediaQuery.of(context).size.height*0.07,
                                            ),
                                            getSignInText(),
                                            getEmail(),
                                            getPassword(),
                                            getSignIn(),
                                            SizedBox(height: MediaQuery.of(context).size.height*0.05,),

                                          ],
                                        ),
                                      ),
                                    )
                                  ]),
                            ))),

                  ],
                ),
              ))),
    );
  }



  Widget getEmail() {
    return Container(
      margin: EdgeInsets.all(5.0),
      /*decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.0)),*/
      child: TextFormField(
          textInputAction: TextInputAction.next,
          keyboardType: TextInputType.emailAddress,
          controller: _email,
          onFieldSubmitted: (v) {
            FocusScope.of(context).requestFocus(focus);
          },
          validator: (val) {
            bool emailValid =
                RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(val);
            if (val.length == 0) {
              return "Please Enter Email Id";
            }
            if (emailValid == false) {
              return "Enter valid Email id";
            }
          },
          decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(
                 0,
                  15,
                  25,
                  15),
              filled: true,
              fillColor: Colors.transparent,
              /*border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    25,
                  )),*/
              hintText: 'Enter Email Id',
              focusColor: Colors.pinkAccent,
              prefixIcon: Icon(
                Icons.person,
                color: Colors.grey,
              )
              /*   enabledBorder: UnderlineInputBorder(
                borderSide: new BorderSide(color: Colors.white),
                borderRadius:
                new BorderRadius.circular(25),
              ),*/
              )),
    );
/*      new Row(
        children: <Widget>[
          Expanded(
              child: Container(
                  margin: EdgeInsets.only(
                      bottom: GlobalFile.GetMinumPAdding + 3.0,
                      right: GlobalFile.GetMinumPAdding),
                  alignment: Alignment.center,

                  child: TextFormField(
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.emailAddress,
                      controller: _email,
                      onFieldSubmitted: (v){
                        FocusScope.of(context).requestFocus(focus);
                      },
                      validator: (val) {
                        if (val.length == 0) {
                          return "Please Enter Email Id" ;
                        }
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.transparent,
                        prefixIcon: Icon(Icons.person_outline),
                        hintText: 'Enter Email Id' ,
                      )
                  )))
        ],
      );*/
  }

  bool _obscureText = true;
  String _password;
  double minimum_padding = 5.0;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  String _contactText;
  bool _passwordVisible;
  String tokenstr = "";

  @override
  void initState() {
    _passwordVisible = false;
    super.initState();
  }

  var focusNode = new FocusNode();


  Widget GetLine() {
    return new Container(
      margin: EdgeInsets.only(left: 4.0, top: 20.0, right: 4.0),
      height: 0.5,
      alignment: Alignment.centerLeft,
      color: Colors.black54,
    );
  }

  Widget getPassword() {
    return Container(
      margin: EdgeInsets.all(5.0),
      /*  decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(25.0)),*/
      child: TextFormField(
          focusNode: focus,
          controller: _pass,
          validator: (value) {
            if (value.length == 0) {
              return "Enter Password";
            }
          },
          onSaved: (val) => _password = val,
          obscureText: _obscureText,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.fromLTRB(
                0.0,
                15,
                25,
                15),
            filled: true,
            fillColor: Colors.transparent,
            /*   border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    25,
                  )),*/
            hintText: 'Enter Password',
            prefixIcon: Icon(Icons.lock_outline),
            /* enabledBorder: UnderlineInputBorder(
                borderSide: new BorderSide(color: Colors.white),
                borderRadius:
                new BorderRadius.circular(25),
              ),*/
            suffixIcon: IconButton(
              onPressed: _toggle,
              icon: Icon(
                // Based on passwordVisible state choose the icon
                _obscureText ? Icons.visibility_off : Icons.visibility,
                color: Colors.black,
              ),
            ),
          )),
    );
/*      new Row(
      children: <Widget>[
        Expanded(
            child: Container(
              margin: EdgeInsets.only(
                  bottom: GlobalFile.GetMinumPAdding + 3.0,
                  right: GlobalFile.GetMinumPAdding),
              alignment: Alignment.center,
              child: TextFormField(
                focusNode: focus,
                controller: _pass,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.transparent,
//                    icon: Icon(Icons.lock_outline),
                    hintText: 'Enter Password',
                    prefixIcon: Icon(Icons.lock_outline),
                    */ /*enabledBorder: UnderlineInputBorder(
                      borderSide: new BorderSide(color: Colors.white),
                      borderRadius: new BorderRadius.circular(25),
                    ),*/ /*
                    suffixIcon: IconButton(
                      onPressed: _toggle,
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _obscureText ? Icons.visibility_off : Icons.visibility,
                        color: Theme.of(context).primaryColorDark,
                      ),
                    )),
                validator: (val) => val.length < 5 ? 'Password too short.' : null,
                onSaved: (val) => _password = val,
                obscureText: _obscureText,
              ),
            ))
      ],
    );*/
  }

  getForgotPass() {
    return new GestureDetector(
        onTap: () {
        },
        child: Padding(
          padding: EdgeInsets.all(
            8.0,
          ),
          child: new Text(
            "Forget Password?",
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.left,
            style: TextStyle(color: Colors.black54, fontSize: 15.0),
          ),
        ));
  }

  Widget getSignIn() {
    return
      new Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child:
          GestureDetector(
            child: Container(
              height: 50.0,
              width: 160.0,
              margin: EdgeInsets.only(top: 12.0,right: 12.0),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: new BorderRadius.circular(8.0),
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [Color(0xfffd5680), Color(0xfffd8c7e)])),
              child: Text(
                "Sign In",
                style: TextStyle(fontSize: 18.0,
                  color: Colors.white,
                ),
              ),
            ),
            onTap: () {
              if (_formkey.currentState.validate()) {
                //SubmitData();
                GlobalFile.setSharedData("login", "1");
                GlobalFile.setSharedData("email", _email.text.toString());
                Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) => new DashBoardActivity()));
              }
            },
          )
          ,
        ),
      ],
    );
  }






  Widget getSignInText() {
    return Center(
        child: Padding(
      padding: EdgeInsets.only(top: 12.0, bottom: 16.0),
      child: Text(
        "Sign in",
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 22.0, color: Colors.black, fontWeight: FontWeight.bold),
      ),
    ));
  }

}
