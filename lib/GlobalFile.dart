import 'package:shared_preferences/shared_preferences.dart';

class GlobalFile
{


  static Future<bool> setSharedData(String key, String value) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, value);
  }

  static Future<String> getSharedData(String key) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? 'en';
  }
}