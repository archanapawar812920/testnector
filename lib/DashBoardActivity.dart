import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Account.dart';
import 'BookingList.dart';

class DashBoardActivity extends StatefulWidget
{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new DashView();
  }

}

class DashView extends State<DashBoardActivity>
{

  static int currentIndex = 0;
  Future<void> onTapped(int value) async {
    if (currentIndex != value) {
      currentIndex = value;

    }
  } // TODO: implement build

  int active=0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: getBody(),
      appBar: new AppBar(title: Text("Pune"),
      backgroundColor: Colors.indigo,

      actions: <Widget>[
        Icon(Icons.search),
        Icon(Icons.filter_list),
      ],),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton:/*FloatingActionButton(
        onPressed: () { },
        tooltip: 'Increment',
        backgroundColor: Colors.red[900],
        child: Icon(Icons.camera_alt,size: 40,),
        elevation: 5.0,

      ),*/
      new Container(
        decoration:  new BoxDecoration(
          color:Colors.red[900],
          shape: BoxShape.circle,
        ),
        height: 60,
        width: 60,
        //color: Colors.red[900],
        child: Icon(Icons.camera_alt,size: 40,color: Colors.white,),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 50,
          child: Row(
           // mainAxisSize: MainAxisSize.max,
           // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: InkWell(
                  onTap: (){
                    active=0;
                    setState(() {

                    });
                  },
                  child: GetBox("Booking",Icons.add_box,active==0?Colors.indigo:Colors.grey),
                ),
              ),
                Expanded(
                  child: InkWell(
                  onTap: (){
                    active=1;
                    setState(() {

                    });
                  },
                  child: GetBox("Live",Icons.local_activity,active==1?Colors.indigo:Colors.grey),
              ),
          ),
              SizedBox(
                width: 30,
              ),

              SizedBox(
                width: 30,
              ),
              Expanded(
                  child: InkWell(
                  onTap: (){
                    active=2;
                    setState(() {

                    });
                  },
                  child: GetBox("History",Icons.hourglass_full,active==2?Colors.indigo:Colors.grey),
              ),
                ), Expanded(
                  child: InkWell(
                  onTap: (){
                    active=3;
                    setState(() {
                    });
                  },
                  child: GetBox("Account",Icons.person,active==3?Colors.indigo:Colors.grey),
              ),
                ),
            ],
          ),
        ),
        //notchedShape: CircularNotchedRectangle(),
        color: Colors.white,
      ),
    );
  }

  GetBox(String s, IconData add_box, Color color1) {
    return Container(
      alignment: Alignment.center,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(add_box,color: color1,size: 25,),
          Text(s,style: TextStyle(color: color1,fontSize: 12),),
        ],
      ),
    );
  }

  getBody() {
    if(active==0)
      {
        return BookingList();
      }if(active==3)
      {
        return Account();
      }else
        {
          new Container();
        }
  }

}