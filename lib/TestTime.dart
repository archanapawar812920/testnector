import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TestTime extends StatefulWidget
{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
  return TestView();
  }

}

class DataModel
{
  String name,avail;
  double value;
  Color c1;

  DataModel(this.name, this.value, this.c1,this.avail);
}
class TestView extends State<TestTime>
{

  @override
  void initState() {
    RefreshList_items.add(new DataModel("07:45PM - 08:00PM",0.2,Colors.green,"Available"));
    RefreshList_items.add(new DataModel("8:00PM - 08:30PM",0.8,Colors.red,"ALMOST FULL"));
    RefreshList_items.add(new DataModel("8:30PM - 09:00PM",0.8,Colors.red,"ALMOST FULL"));
    RefreshList_items.add(new DataModel("8:30PM - 09:00PM",0.8,Colors.red,"ALMOST FULL"));
    RefreshList_items.add(new DataModel("9:00PM - 09:30PM",0.5,Colors.yellow,"FILLING FAST"));
    RefreshList_items.add(new DataModel("9:30PM - 10:30PM",0.5,Colors.yellow,"FILLING FAST"));
  }

  List<DataModel> RefreshList_items=new List();
  getListing()
  {
    return new ListView.builder(
        itemCount: RefreshList_items.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (BuildContext ctxt, int index) {
          return new InkWell(
            onTap: () {
            },
            child: new Container(
              child: new Column(
                children: [
                  Divider(thickness: 2.0,),
                  new Container(
                    padding: EdgeInsets.all(20.0),
                    child:  new Row(
                      children: [
                        Expanded(flex: 8,child: new Container(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(RefreshList_items[index].name,style: TextStyle(fontSize: 16,color: Colors.black),),
                              SizedBox(height: 5,),


                              LinearProgressIndicator(
                                backgroundColor: Colors.grey[100],
                                valueColor: AlwaysStoppedAnimation<Color>(RefreshList_items[index].c1,),
                                value: RefreshList_items[index].value,
                              ),
                              SizedBox(height: 5,),
                              new Container(
                                alignment: Alignment.center,
                                child:  Text(RefreshList_items[index].avail,style: TextStyle(fontSize: 14,color: Colors.grey),textAlign: TextAlign.center,),
                              ),

                            ],
                          ),
                        ),),
                        SizedBox(width: 20,),

                        Expanded(flex: 2,
                          child: new Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(10.0),
                            color: Colors.red[900],
                            child:Text("JOIN",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
                            ),
                          ),),
                      ],
                    ),
                  ),

                ],
              ),
            ),
          );
        });
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
     return Scaffold(
       body: DefaultTabController(

         length: 3,
         child: Scaffold(

           appBar: AppBar(
             backgroundColor: Colors.red[800],
             bottom: TabBar(
               onTap: (index) {

               },
               tabs: [
                 /*Tab(icon: Icon(Icons.card_travel),
                   text: "Today",),*/
                 new Column(
                   children: [
                     Text("05",style: TextStyle(color: Colors.white,fontSize: 20),),
                     Text("Today",style: TextStyle(color: Colors.white),),
                     SizedBox(height: 10,)
                   ],
                 ), new Column(
                   children: [
                     Text("05",style: TextStyle(color: Colors.white,fontSize: 20),),
                     Text("Tomarrow",style: TextStyle(color: Colors.white),),
                     SizedBox(height: 10,)
                   ],
                 ),
                 new Column(
                   children: [
                     Icon(Icons.calendar_today),
                     Text("Tomarrow",style: TextStyle(color: Colors.white),),
                     SizedBox(height: 10,)
                   ],
                 ),

               ],
             ),
             title: Text('Choose a time slot'),
           ),

           body: TabBarView(
             children: [

               SingleChildScrollView(
                 child: Card(
                   child:ExpansionTile(
                     title: Text("6PM - 9:30PM",style: TextStyle(color: Colors.black),),
                     initiallyExpanded: true,

                     children: [
                       getListing(),
                     ],
                   ),

                 ),
               ),

               Center(
                   child: Text(
                     "1",
                     style: TextStyle(fontSize: 40),
                   )),Center(
                   child: Text(
                     "2",
                     style: TextStyle(fontSize: 40),
                   )),
             ],
           ),
         ),
       ),
     );
  }

}